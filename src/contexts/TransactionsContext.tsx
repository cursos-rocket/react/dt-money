import { ReactNode, useEffect, useState, useCallback } from "react";
import { api } from "../lib/axios";
import { createContext } from "use-context-selector";

interface Transaction {
    id: number;
    description: string;
    type: "income"|"outcome";
    category: string;
    price: number;
    createdAt: string;
}

interface CreateTransactionInput {
    description: string;
    type: "income"|"outcome";
    category: string;
    price: number;
}

interface TransactionsContextType {
    transactions: Transaction[];
    getTransactions: (query ?: string) => Promise<void>
    createTransaction: (data: CreateTransactionInput) => Promise<void>
}

interface TransactionsProviderProps {
    children: ReactNode;
}

export const TransactionsContext = createContext({} as TransactionsContextType);

export function TransactionsProvider({ children }: TransactionsProviderProps) {

    const [transactions, setTransactions] = useState<Transaction[]>([]);

    const getTransactions = useCallback(async (query?: string) => {
        const response = await api.get('/transactions', {
            params: {
                q: query,
                _sort: 'createdAt',
                _order: 'desc'
            }
        });        
        setTransactions(response.data);
    }, []);

   const createTransaction = useCallback(async (data: CreateTransactionInput) => {
        const {description, price, category, type} = data;
        const response = await api.post('/transactions', {
            description, 
            price,
            category,
            type,
            createdAt: new Date()
        });

        setTransactions(state => [response.data, ...state]);
    }, []);

    useEffect(() => {
        getTransactions();
    }, [getTransactions]);

    return (
        <TransactionsContext.Provider value={
            {
                transactions,
                getTransactions,
                createTransaction
            }
        }>
            {children}
        </TransactionsContext.Provider>
    );
}